{
  description = "Telegram Moeru Bot";

  inputs = {
    nixpkgs.url = "github:NixOS/nixpkgs/nixpkgs-unstable";
    flake-utils.url = "github:numtide/flake-utils";
    poetry2nix = {
      url = "github:nix-community/poetry2nix";
      inputs.nixpkgs.follows = "nixpkgs";
    };
  };

  outputs = { self, nixpkgs, flake-utils, poetry2nix } @ inputs:
    flake-utils.lib.eachDefaultSystem (system:
      let
        pkgs = nixpkgs.legacyPackages.${system};
        poetry2nix_ = poetry2nix.lib.mkPoetry2Nix { inherit pkgs; };
        dev-shell-build-inputs = [
          pkgs.pyright
          pkgs.python3Packages.autopep8
          pkgs.poetry
          pkgs.chromedriver
          pkgs.chromium
        ];
        poetry-overrides = poetry2nix_.overrides.withDefaults (self: super: {
          attrs = super.attrs.overridePythonAttrs (
            old: {
              nativeBuildInputs = (old.nativeBuildInputs or [ ]) ++ [
                self.hatchling
                self.hatch-fancy-pypi-readme
                self.hatch-vcs
              ];
            }
          );
        });
      in
      rec {
        packages.default = poetry2nix_.mkPoetryApplication {
          projectDir = ./.;
          propagatedBuildInputs = [ pkgs.chromedriver pkgs.chromium ];
          overrides = poetry-overrides;
        };

        devShells.default = (poetry2nix_.mkPoetryEnv {
          projectDir = ./.;
          overrides = poetry-overrides;
        }).env.overrideAttrs (oldAttrs: {
          buildInputs = dev-shell-build-inputs;
        });
      }) // {
        nixosModules.default = import ./nixos/moerud.nix {
          moeru-bot-packages = self.packages;
        };
      };
}
