from __future__ import annotations
import asyncio
from time import sleep
from typing import List
from urllib.parse import urljoin, urlparse
import logging
from dataclasses import dataclass
import re

from selenium import webdriver
from selenium.common.exceptions import NoSuchElementException, TimeoutException
from selenium.webdriver.chrome.options import Options
from selenium.webdriver.common.by import By
from selenium.webdriver.remote.webelement import WebElement
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC

import requests


def strip_qeruy_params(url: str) -> str:
    return urljoin(url, urlparse(url).path)


def canonicalize_twitter_url(url: str) -> str:
    url = strip_qeruy_params(url)
    url = url.replace("x.com", "twitter.com")
    url = re.sub("/photo/\d+$", "", url)
    return url


@dataclass
class SourcedImage:
    data: bytes
    source_url: str
    extension: str


class Fetcher:
    def __init__(self, headless: bool = True) -> None:
        options = Options()
        options.headless = headless
        self._driver = webdriver.Chrome(options=options)
        self._driver.set_window_size(1920, 1080)
        self._pixiv_logged_in = False

    async def login_pixiv(self, php_sess_id: str, device_token: str):
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, lambda: self._login_pixiv(php_sess_id, device_token))

    def _login_pixiv(self, php_sess_id: str, device_token: str):
        if self._pixiv_logged_in:
            return

        self._driver.get("https://pixiv.net")
        self._driver.add_cookie({
            "name": "PHPSESSID",
            "value": php_sess_id,
            "domain": ".pixiv.net",
        })
        self._driver.add_cookie({
            "name": "device_token",
            "value": device_token,
            "domain": ".pixiv.net",
        })

        self._pixiv_logged_in = True

    async def download_pixiv_images(self, url: str) -> List[SourcedImage]:
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, lambda: self._download_pixiv_images(url))

    def _download_pixiv_images(self, url: str) -> List[SourcedImage]:
        url = strip_qeruy_params(url)
        self._driver.get(url)
        srcs = self._pixiv_find_thumbnail_img_srcs()

        logging.info(f"Got pixiv image URLs: {srcs}")

        images = []
        for i, src in enumerate(srcs):
            logging.info(f"Downloading img at index {i}")
            r = requests.get(
                src, headers={"referer": "https://accounts.pixiv.net/"})
            extension = src.split(".")[-1]
            images.append(SourcedImage(data=r.content,
                          source_url=url, extension=extension))

        return images

    def _pixiv_find_thumbnail_img_srcs(self) -> List[str]:
        anchor_elems = self._pixiv_find_thumbnail_anchor_elems()
        return list(map(lambda img: img.get_attribute("href"), anchor_elems))

    def _pixiv_find_thumbnail_anchor_elems(self) -> List[WebElement]:
        first_img_elem = WebDriverWait(self._driver, 10).until(
            EC.presence_of_element_located((By.CSS_SELECTOR, "figure img")))
        first_img_elem.click()

        sleep(5)

        elems = WebDriverWait(self._driver, 10).until(
            EC.presence_of_all_elements_located((By.CSS_SELECTOR, "figure a")))
        return elems

    async def download_twitter_images(self, url: str) -> List[SourcedImage]:
        loop = asyncio.get_event_loop()
        return await loop.run_in_executor(None, lambda: self._download_twitter_images(url))

    def _download_twitter_images(self, url: str) -> List[SourcedImage]:
        """
        url: URL with unstripped query parameters.  The parameters are stripped before navigating.
        """

        url = canonicalize_twitter_url(url)
        self._driver.get(url)
        srcs = self._twitter_find_thumbnail_img_srcs()

        logging.info(f"Got twitter image URLs: {srcs}")

        images = []
        for i, src in enumerate(srcs):
            logging.info(f"Downloading img at index {i}")
            r = requests.get(strip_qeruy_params(src), params={
                             "format": "png", "name": "large"})
            images.append(SourcedImage(data=r.content,
                          source_url=url, extension="png"))

        return images

    def _twitter_find_thumbnail_img_srcs(self) -> List[str]:
        thumbnail_img_elems = self._twitter_find_thumbnail_img_elems()
        return list(map(lambda img: img.get_attribute("src"), thumbnail_img_elems))

    def _twitter_find_thumbnail_img_elems(self) -> List[WebElement]:
        # This assumes that the "main tweet" has tabindex -1
        view_button_selector = 'article[tabindex="-1"] div[role=presentation] div[role=button][tabindex="0"]'
        # This selects the thumbnail images AND the <time> tags.
        # The <img> elements after the <time> tags are discarded.
        thumbnail_selector = 'article img[src^="https://pbs.twimg.com/media"], time'

        try:
            # Click the "view" button if present
            view_buttons = WebDriverWait(self._driver, 5).until(
                EC.presence_of_all_elements_located(
                    (By.CSS_SELECTOR, view_button_selector))
            )

            if len(view_buttons) == 1:
                view_buttons[0].click()
            elif len(view_buttons) > 1:
                logging.warn("More than one sensitive view button selected")
                view_buttons[0].click()
        except TimeoutException:
            logging.info(
                "No view button found, proceeding to get thumbnail elements")

        # Wait for all thumbnail elements
        elements = WebDriverWait(self._driver, 10).until(
            EC.presence_of_all_elements_located(
                (By.CSS_SELECTOR, thumbnail_selector))
        )
        first_time_tag_index = next(idx for idx, elem in enumerate(
            elements) if elem.tag_name == 'time')
        return elements[:first_time_tag_index]
