from __future__ import annotations

from json import loads, dumps
from typing import List, cast
import asyncio
import logging
import os
import re
import traceback
import html
import hashlib

from telegram import Bot, Document, InlineKeyboardButton, InlineKeyboardMarkup, MessageEntity, PhotoSize, Update
from telegram.constants import ParseMode
from telegram.ext import ApplicationBuilder, CallbackQueryHandler, CommandHandler, ContextTypes, MessageHandler, filters
import telegram

import aiofiles

from .fetcher import Fetcher, SourcedImage, strip_qeruy_params
from .image_resize import compress_image

logging.basicConfig(
    format='%(asctime)s - %(name)s - %(levelname)s - %(message)s',
    level=logging.INFO
)

MOERU_BOT_TOKEN = os.environ['MOERU_BOT_TOKEN']
MOERU_BOT_CHANNEL = os.environ['MOERU_BOT_CHANNEL']
MOERU_BOT_ADMIN = os.environ['MOERU_BOT_ADMIN']
MOERU_BOT_PIXIV_CHECKHEALTH_URL = os.environ.get(
    'MOERU_BOT_PIXIV_CHECKHEALTH_URL') or "https://www.pixiv.net/artworks/103912045"
MOERU_BOT_DEBUG = os.environ.get('MOERU_BOT_DEBUG')
FETCHER_HEADLESS = True if MOERU_BOT_DEBUG is None else False

fetcher_lock = asyncio.Lock()

global_state = {
    "php_sess_id": "",
    "device_token": "",
    "admin_chat_id": None,
}

if os.path.isfile("state.json"):
    with open("state.json", "r") as f:
        global_state |= loads(f.read())


async def save_global_state():
    async with aiofiles.open("state.json", "w") as f:
        await f.write(dumps(global_state))


def main() -> None:
    application = ApplicationBuilder().token(MOERU_BOT_TOKEN).build()

    asyncio.get_event_loop().create_task(pixiv_checkhealth_periodic(application.bot))

    pixiv_cmd_handler = CommandHandler(
        command='pixiv', callback=on_pixiv_command)
    application.add_handler(pixiv_cmd_handler)

    admin_cmd_handler = CommandHandler(
        command='admin', callback=on_admin_command)
    application.add_handler(admin_cmd_handler)

    checkhealth_cmd_handler = CommandHandler(
        command='checkhealth', callback=on_checkhealth_command)
    application.add_handler(checkhealth_cmd_handler)

    msg_handler = MessageHandler(
        filters=filters.ALL, callback=on_msg, block=True)
    application.add_handler(msg_handler)

    prompt_callback_handler = CallbackQueryHandler(
        callback=on_prompt_callback, pattern=re.compile("^(approve|deny)$"))
    application.add_handler(prompt_callback_handler)

    application.add_error_handler(error_handler)

    application.run_polling()


async def on_msg(update: Update, context: ContextTypes.DEFAULT_TYPE):
    if not update.message: return
    if not update.message.from_user: return
    if not update.message.from_user.username: return
    if not update.message.text: return
    if not update.effective_chat: return
    if not update.effective_chat.id: return

    from_username = update.message.from_user.username
    if from_username != MOERU_BOT_ADMIN:
        logging.info(f"User @{from_username} is not the admin")
        return

    # Extract URLs from the message
    url_entities = filter(msg_entity_has_url, update.message.entities)
    if not url_entities:
        logging.info(f"Message does not contain any URL")
        return
    logging.info(f"Found these urls from the message: {url_entities}")

    text = update.message.text
    urls = list(map(lambda entity: msg_entity_get_url(
        entity, text), url_entities))

    if update.message.reply_to_message and update.message.reply_to_message.photo:
        # If the messages replies to a manually-uploaded image,
        # proceed to download and send the image directly to the channel
        url = strip_qeruy_params(urls[0])
        asyncio.create_task(download_and_send_to_channel(update.message.reply_to_message.photo[-1], url, context))
    else:
        asyncio.create_task(download_and_prompt_images(
            urls, update.effective_chat.id, context))


async def download_and_prompt_images(
        urls: List[str],
        admin_chat_id: int,
        context: ContextTypes.DEFAULT_TYPE
) -> None:
    try:
        async with fetcher_lock:
            await _download_and_prompt_images(urls, admin_chat_id, context)
    except Exception as e:
        await report_exception(context.bot, e)


async def _download_and_prompt_images(
        urls: List[str],
        admin_chat_id: int,
        context: ContextTypes.DEFAULT_TYPE
) -> None:
    # Scrape image urls and download images
    fetcher = Fetcher(headless=FETCHER_HEADLESS)
    images: List[SourcedImage] = []
    for url in urls:
        logging.info(f"Fetching from page url {url}")
        if is_twitter_url(url):
            try:
                new_images = await fetcher.download_twitter_images(url)
                if not new_images:
                    raise NoImageException()
                images.extend(new_images)
            except NoImageException:
                logging.warn(f"No image fetched from url {url}")
                await context.bot.send_message(
                    chat_id=admin_chat_id,
                    text=f"No image fetched from url {url}",
                )
            except Exception as e:
                await report_exception(context.bot, e)
        elif is_pixiv_url(url):
            try:
                await fetcher.login_pixiv(global_state['php_sess_id'], global_state['device_token'])
                new_images = await fetcher.download_pixiv_images(url)
                if not new_images:
                    raise NoImageException()
                images.extend(new_images)
            except NoImageException:
                logging.warn(f"No image fetched from url {url}")
                await context.bot.send_message(
                    chat_id=admin_chat_id,
                    text=f"No image fetched from url {url}",
                )
            except Exception as e:
                await report_exception(context.bot, e)

    # Save original images
    logging.info("Saving original images")
    os.makedirs("archives/", exist_ok=True)
    for image in images:
        image_sha1 = hashlib.sha1(image.data).hexdigest()
        image_path = f"archives/{image_sha1}.{image.extension}"
        logging.info(f"Saving image to path {image_path}")
        async with aiofiles.open(f"archives/{image_sha1}.{image.extension}", "wb") as f:
            await f.write(image.data)

    # Compress images
    logging.info("Compressing images")
    for image in images:
        logging.info(f"Compressing an image from source {image.source_url}")
        image.data = compress_image(image.data)

    # Send prompt messages to admin
    for image in images:
        logging.info(
            f"Sending prompt of an image from source {image.source_url}")
        await context.bot.send_document(
            chat_id=admin_chat_id,
            document=image.data,
            caption=image.source_url,
            filename=f"preview.png",
            reply_markup=InlineKeyboardMarkup([[
                InlineKeyboardButton("Approve", callback_data="approve"),
                InlineKeyboardButton("Deny", callback_data="deny"),
            ]]),
        )


async def on_prompt_callback(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    if not update.callback_query: return
    if not update.callback_query.data: return
    if not update.callback_query.message: return
    if not update.callback_query.message.document: return
    if not update.callback_query.message.caption: return
    if not update.effective_chat: return
    if not update.effective_chat.id: return
    if not update.effective_user: return
    if not update.effective_user.username: return

    await update.callback_query.answer()

    if update.effective_user.username != MOERU_BOT_ADMIN:
        logging.warn(
            f"Received prompt from non-admin user: {update.effective_user.username}")
        return

    source_url = update.callback_query.message.caption

    if update.callback_query.data == "approve":
        logging.info(f"Approving an image from url {source_url}")
        document = update.callback_query.message.document
        asyncio.create_task(download_and_send_to_channel(
            document, source_url, context))

    # Delete the prompt message
    logging.info(f"Deleting an image prompt from url {source_url}")
    admin_chat_id = update.effective_chat.id
    prompt_msg_id = update.callback_query.message.id
    await context.bot.delete_message(chat_id=admin_chat_id, message_id=prompt_msg_id)


async def download_and_send_to_channel(
        document: Document | PhotoSize,
        source_url: str,
        context: ContextTypes.DEFAULT_TYPE,
) -> None:
    try:
        source_text = map_source_url_to_text(source_url)
        file = await document.get_file()
        image_bytes = bytes(await file.download_as_bytearray())
        await context.bot.send_photo(
            chat_id=MOERU_BOT_CHANNEL,
            parse_mode=telegram.constants.ParseMode.MARKDOWN_V2,
            caption=f"[{source_text}]({source_url})",
            photo=image_bytes,
            disable_notification=True,
        )
    except Exception as e:
        await report_exception(context.bot, e)


def msg_entity_has_url(entity: MessageEntity) -> bool:
    return entity.type == MessageEntity.URL or entity.type == MessageEntity.TEXT_LINK


def msg_entity_get_url(entity: MessageEntity, text: str) -> str:
    if entity.type == MessageEntity.URL:
        return text[entity.offset:entity.offset + entity.length]
    elif entity.type == MessageEntity.TEXT_LINK:
        # Text links always have url attribute
        return cast(str, entity.url)
    raise RuntimeError(
        "msg_entity_get_url called with entity type without an url")


async def on_pixiv_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logging.info("/pixiv command issued")

    if not update.message: return
    if not update.message.text: return
    if not update.message.from_user: return
    if not update.message.from_user.username: return
    if not update.effective_chat: return
    if not update.effective_chat.id: return
    if not update.effective_chat.type: return

    if update.effective_chat.type != telegram.constants.ChatType.PRIVATE:
        logging.warn("Received /pixiv command from non-private type chat")
        return

    if update.message.from_user.username != MOERU_BOT_ADMIN:
        logging.warn(
            f"Received /pixiv command from non-admin user: {update.message.from_user.username}")
        return

    parts = update.message.text.strip().split()
    if len(parts) != 3:
        await context.bot.send_message(
            chat_id=update.effective_chat.id,
            text="Usage: `/pixiv <PHPSESSID> <device_token>`",
            parse_mode=ParseMode.MARKDOWN_V2,
        )
        return

    _, php_sess_id, device_token = parts
    logging.info(f"Updating pixiv cookies to {php_sess_id}, {device_token}")

    global_state['php_sess_id'] = php_sess_id
    global_state['device_token'] = device_token
    await save_global_state()

    await context.bot.send_message(chat_id=update.effective_chat.id, text="Done updating pixiv cookies")


async def on_admin_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logging.info("/admin command issued")

    if not update.message: return
    if not update.message.from_user: return
    if not update.message.from_user.username: return
    if not update.effective_chat: return
    if not update.effective_chat.id: return
    if not update.effective_chat.type: return

    if update.effective_chat.type != telegram.constants.ChatType.PRIVATE:
        logging.warn("Received /admin command from non-private type chat")
        return

    if update.message.from_user.username != MOERU_BOT_ADMIN:
        logging.warn(
            f"Received /admin command from non-admin user: {update.message.from_user.username}")
        return

    logging.info(f"Updating admin chat id to: {update.effective_chat.id}")
    global_state["admin_chat_id"] = update.effective_chat.id
    await save_global_state()

    await context.bot.send_message(chat_id=update.effective_chat.id, text="Done updating admin chat id")


async def on_checkhealth_command(update: Update, context: ContextTypes.DEFAULT_TYPE) -> None:
    logging.info("/checkhealth command issued")

    if not update.message: return
    if not update.message.text: return
    if not update.message.from_user: return
    if not update.message.from_user.username: return
    if not update.effective_chat: return
    if not update.effective_chat.id: return
    if not update.effective_chat.type: return

    if update.effective_chat.type != telegram.constants.ChatType.PRIVATE:
        logging.warn(
            "Received /checkhealth command from non-private type chat")
        return

    if update.message.from_user.username != MOERU_BOT_ADMIN:
        logging.warn(
            f"Received /checkhealth command from non-admin user: {update.message.from_user.username}")
        return

    parts = update.message.text.strip().split()
    if len(parts) == 2:
        url = parts[1]
    else:
        url = MOERU_BOT_PIXIV_CHECKHEALTH_URL

    asyncio.get_event_loop().create_task(pixiv_checkhealth(context.bot, url, verbose=True))


def is_twitter_url(url: str) -> bool:
    return "twitter.com" in url or "x.com" in url


def is_pixiv_url(url: str) -> bool:
    return "pixiv.net" in url


def map_source_url_to_text(url: str) -> str:
    if is_twitter_url(url):
        return "source \\(twitter\\)"
    elif is_pixiv_url(url):
        return "source \\(pixiv\\)"
    else:
        raise RuntimeError(f"Unknown source for url: {url}")


async def pixiv_checkhealth_periodic(bot: Bot) -> None:
    # Avoid performing health check on startup
    await asyncio.sleep(60 * 60 * 3)
    while True:
        await pixiv_checkhealth(bot, MOERU_BOT_PIXIV_CHECKHEALTH_URL)
        await asyncio.sleep(60 * 60 * 24)


async def pixiv_checkhealth(bot: Bot, url: str, verbose: bool = False) -> None:
    logging.info("Performing Pixiv cookie health check")
    try:
        async with fetcher_lock:
            images = await _pixiv_checkhealth(url)

        if (not verbose) or (not global_state["admin_chat_id"]):
            del images
            return

        await bot.send_message(chat_id=global_state["admin_chat_id"], text=f"Pixiv checkhealth suceeded with url: {url}")
        for image in images:
            await bot.send_document(
                chat_id=global_state["admin_chat_id"],
                document=image.data,
                caption="Pixiv checkhealth result"
            )

    except Exception as e:
        await bot.send_message(chat_id=global_state["admin_chat_id"], text=f"Pixiv checkhealth failed to download file from url: {url}")
        await report_exception(bot, e)


async def _pixiv_checkhealth(url: str) -> List[SourcedImage]:
    fetcher = Fetcher(headless=FETCHER_HEADLESS)
    await fetcher.login_pixiv(php_sess_id=global_state["php_sess_id"], device_token=global_state["device_token"])
    images = await fetcher.download_pixiv_images(url)
    for image in images:
        n_megabyte = 2**20
        if len(image.data) < n_megabyte:
            raise RuntimeError(
                "Fetched health check image has size less than 1 MB")
    return images


async def error_handler(update: object, context: ContextTypes.DEFAULT_TYPE) -> None:
    """Log the error and send a telegram message to notify the developer."""
    logging.error("Exception while handling an update:",
                  exc_info=context.error)
    assert context.error
    await report_exception(context.bot, context.error)


async def report_exception(bot: Bot, exc: Exception) -> None:
    logging.error("Exception:", exc_info=exc)

    if not global_state["admin_chat_id"]:
        logging.warn("No globally-saved admin chat id")
        return

    tb_list = traceback.format_exception(None, exc, exc.__traceback__)
    tb_string = "".join(tb_list)

    message = (
        f"An exception was raised:\n"
        f"<pre>{html.escape(tb_string)}</pre>"
    )

    await bot.send_message(
        chat_id=global_state["admin_chat_id"], text=message, parse_mode=ParseMode.HTML
    )


class NoImageException(Exception):
    pass
