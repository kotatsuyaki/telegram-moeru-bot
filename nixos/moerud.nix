{ moeru-bot-packages }:

{ config, lib, options, pkgs, ... }:

let
  cfg = config.services.moerud;
  moeru-bot = moeru-bot-packages.${pkgs.system}.default;
in
{
  options.services.moerud = {
    enable = lib.mkEnableOption "moerud";
    environmentFile = lib.mkOption {
      description = "File from which environment variables are loaded.  See systemd.exec(5).";
      type = lib.types.path;
      default = "/secrets/moerud.env";
    };
  };

  config = lib.mkIf cfg.enable {
    systemd.services.moerud = {
      description = "Moeru Telegram bot service";
      wants = [
        "network-online.target"
      ];
      serviceConfig = {
        DynamicUser = true;
        Restart = "always";
        ExecStart = "${moeru-bot}/bin/moeru-bot";
        StateDirectory = "moerud";
        WorkingDirectory = "%S/moerud"; # The StateDirectory
        EnvironmentFile = cfg.environmentFile;
      };
    };
  };
}
